package com.pms.sdk;

public interface IPMSConsts {

	public static final String PMS_VERSION = "2.0.6";

	public final static String PMS_PROPERY_NAME = "pms.properties";

	// [start] GCM
	public static final String KEY_APP = "app";
	public static final String KEY_SENDER = "sender";
	public static final String KEY_GCM_PROJECT_ID = "gcm_project_id";
	public static final String KEY_GCM_TOKEN = "registration_id";
	public static final String DB_UUID = "uuid";

	public static final String URL_GOOGLE_CLIENT = "https://www.google.com/accounts/ClientLogin";
	public static final String URL_GOOGLE_SENDER = "https://android.apis.google.com/c2dm/send";

	public static final String ACTION_REGISTER = "com.google.android.c2dm.intent.REGISTER";
//	public static final String ACTION_REGISTRATION = "com.google.android.c2dm.intent.REGISTRATION";
	public static final String ACTION_RECEIVE = "com.google.android.fcm.intent.RECEIVE";
	public static final String ACTION_PUSH_RECEIVE = "com.pms.sdk.push.RECEIVER";
	public static final String GSF_PACKAGE = "com.google.android.gsf";

	public static final String GCMRECIVER_PATH = "com.pms.sdk.push.gcm.GCMReceiver";

	public static final String POLLING_KEY = "DATA";
	// [end] GCM

	// [start] MQTT
	public static final String ACTION_MQTT_BUNDLE = "MQTT_BUNDLE";
	public static final String ACTION_START = "MQTT.START";
	public static final String ACTION_FORCE_START = "MQTT.FORCE_START";
	public static final String ACTION_STOP = "MQTT.STOP";
	public static final String ACTION_RESTART = "MQTT.RESTART";
	public static final String ACTION_RECONNECT = "MQTT.RECONNECT";
	public static final String ACTION_CHANGERECONNECT = "MQTT.CHANGERECONNECT";
	public static final String ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
	public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
	public static final String ACTION_ACTION_PACKAGE_RESTARTED = "android.intent.action.ACTION_PACKAGE_RESTARTED";
	// [end] MQTT

	// [start] PUSH
	public static final String KEY_MSG_ID = "i";
	public static final String KEY_NOTI_TITLE = "notiTitle";
	public static final String KEY_NOTI_MSG = "notiMsg";
	public static final String KEY_NOTI_IMG = "notiImg";
	public static final String KEY_MSG = "message";
	public static final String KEY_SOUND = "sound";
	public static final String KEY_MSG_TYPE = "t";
	public static final String KEY_DATA = "d";
	// [end] PUSH

	// [start] Receiver
	public static final String RECEIVER_PUSH = "com.pms.sdk.push";
	public static final String RECEIVER_REQUERY = "com.pms.sdk.requery";
	// public static final String RECEIVER_REFRESH_MSG_GRP = "com.pms.sdk.refreshmsggrp";
	// public static final String RECEIVER_REFRESH_MSG = "com.pms.sdk.refreshmsg";
	// public static final String RECEIVER_REFRESH_BADGE = "com.pms.sdk.refreshbadge";
	public static final String RECEIVER_CLOSE_INBOX = "com.pms.sdk.closeinbox";
	public static final String RECEIVER_NOTIFICATION = "com.pms.sdk.notification";
	public static final String DEFAULT_PUSH_POPUP_ACTIVITY = "com.pms.sdk.push.PushPopupActivity";
	// [end] Receiver

	// [start] Preferences
	public static final String PREF_FILE_NAME = "pref_pms";

	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";
	public static final String PROTOCOL_SSL = "S";
	public static final String PROTOCOL_TCP = "T";

	public static final String PREF_CUST_ID = "cust_id";
	public static final String PREF_LOGINED_STATE = "logined_state";
	public static final String PREF_UUID = "uuid";
	public static final String PREF_LOGINED_CUST_ID = "logined_cust_id";
	public static final String PREF_MAX_USER_MSG_ID = "max_user_msg_id";
	public static final String PREF_SSL_SIGN_KEY = "ssl_sign_key";
	public static final String PREF_SSL_SIGN_PASS = "ssl_sign_pass";

	public static final String PREF_MQTT_FLAG = "mqtt_flag";

	public static final String PREF_CLICK_LIST = "click_list";
	public static final String PREF_READ_LIST = "read_list";

	public static final String PREF_MSG_FLAG = "msg_flag";
	public static final String PREF_NOTI_FLAG = "noti_flag";
	public static final String PREF_MKT_FLAG = "mkt_flag";
	public static final String PREF_INFO_FLAG = "info_flag";
	public static final String PREF_API_LOG_FLAG = "api_log_flag";
	public static final String PREF_PRIVATE_LOG_FLAG = "private_log_flag";
	public static final String PREF_PRIVATE_FLAG = "private_flag";
	public static final String PREF_ALERT_FLAG = "alert_flag";
	public static final String PREF_RING_FLAG = "ring_flag";
	public static final String PREF_VIBE_FLAG = "vibe_flag";
	public static final String PREF_SCREEN_WAKEUP_FLAG = "screen_wakeup_flag";
	public static final String PREF_PRIVATE_PROTOCOL = "private_protocol";
	public static final String PREF_ISPOPUP_ACTIVITY = "ispopup_activity";
	public static final String PREF_GCM_FLAG = "gcm_flag";

	public static final String PREF_INBOX_ACTIVITY = "pref_inbox_activity";
	public static final String PREF_PUSH_POPUP_ACTIVITY = "pref_push_popup_activity";
	public static final String PREF_PUSH_POPUP_SHOWING_TIME = "pref_push_popup_showing_time";
	public static final String PREF_PUSH_POPUP_COLOR = "pref_push_popup_color";
	public static final String PREF_PUSH_POPUP_BACKGROUND_COLOR = "pref_push_popup_background_color";

	public static final String PREF_POPUP_CONFIRM_TEXT = "popup_confirm_text";
	public static final String PREF_POPUP_CANCEL_TEXT = "popup_cancel_text";

	public static final String PREF_NOTI_SOUND = "noti_sound";
	public static final String PREF_NOTI_RECEIVER_CLASS = "noti_receiver_cls";
	public static final String PREF_NOTI_RECEIVER = "noti_receiver";
	public static final String PREF_PUSH_RECEIVER = "push_receiver_action";

	public static final String PREF_DEVICECERT_STATUS = "pref_devicecert_status";

	public static final String PREF_ONEDAY_LOG = "pref_oneday_log";
	public static final String PREF_YESTERDAY = "pref_yesterday";
	// [end] Preferences

	// [start] api
	// default encrypt key
	public static final String DEFAULT_ENC_KEY = "Pg-s_E_n_C_k_e_y";
	// API SERVER URL
	public static final String API_SERVER_URL = "http://119.207.76.92:8092/msg-api/";
	// time
	public static final long EXPIRE_RETAINED_TIME = 30/* minute */* 60 * 1000;
	public static final String PREF_LAST_API_TIME = "pref_last_api_time";
	// URL list
	public static final String API_DEVICE_CERT = "deviceCert.m";
	public static final String API_COLLECT_LOG = "collectLog.m";
	public static final String API_LOGIN_PMS = "loginPms.m";
	public static final String API_NEW_MSG = "newMsg.m";
	public static final String API_READ_MSG = "readMsg.m";
	public static final String API_CLICK_MSG = "clickMsg.m";
	public static final String API_SET_CONFIG = "setConfig.m";
	public static final String API_LOGOUT_PMS = "logoutPms.m";
	public static final String API_GET_SIGNKEY = "getSignKey.m";

	// pref
	public static final String PREF_SERVER_URL = "pref_server_url";
	public static final String PREF_APP_USER_ID = "pref_app_user_id";
	public static final String PREF_ENC_KEY = "pref_enc_key";
	// request params(json) key
	public static final String KEY_API_DEFAULT = "d";
	public static final String KEY_APP_USER_ID = "id";
	public static final String KEY_ENC_PARAM = "data";
	// result params(json) key
	public static final String KEY_API_CODE = "code";
	public static final String KEY_API_MSG = "msg";
	// result code (server)
	public static final String CODE_SUCCESS = "000";
	public static final String CODE_NULL_PARAM = "100";
	public static final String CODE_WRONG_SIZE_PARAM = "101";
	public static final String CODE_WRONG_PARAM = "102";
	public static final String CODE_DECRPYT_FAIL = "103";
	public static final String CODE_PARSING_JSON_ERROR = "104";
	public static final String CODE_WRONG_SESSION = "105";
	public static final String CODE_ENCRYPT_ERROR = "106";
	public static final String CODE_API_INVALID = "109";
	public static final String CODE_INNER_ERROR = "200";
	public static final String CODE_EXTRA_ERROR = "201";
	// result code (client)
	public static final String CODE_SESSION_EXPIRED = "901";
	public static final String CODE_CONNECTION_ERROR = "902";
	public static final String CODE_NOT_RESPONSE = "903";
	public static final String CODE_URL_IS_NULL = "904";
	public static final String CODE_PARAMS_IS_NULL = "905";
	public static final String CODE_PERMISSION_DENIED_PHONE_STATE ="908";

	// device cert
	public static final String NO_TOKEN = "noToken";
	// new msg type
	public static final String TYPE_PREV = "P";
	public static final String TYPE_NEXT = "N";
	// auth status
	public static final String DEVICECERT_PENDING = "devicecert_pending";
	public static final String DEVICECERT_PROGRESS = "devicecert_progress";
	public static final String DEVICECERT_FAIL = "devicecert_fail";
	public static final String DEVICECERT_COMPLETE = "devicecert_complete";
	// [end] api

	// [start] Property key
	public static final String PRO_ENABLE_TMS_UUID = "enable_PmsUUID";
	// [end] Property key

	// [start] Job scheduler
	public static final int JOB_SCHEDULER_ID = 0x1000;
	public static final int JOB_SCHEDULER_ID_NETWORK_CHANGED = 0x1001;
	public static final long JOB_SCHEDULER_INTERVAL = 1000;
	// [end] Job scheduler
	
	public static final String SERVICE_SET_DEBUG = "service_set_debug";

	public static final String PREF_PUSH_RECEIVER_CLASS = "push_receiver_class";
	public static final String DB_NOTI_CHANNEL_ID = "noti_channel_id";
	public static final String META_DATA_NOTI_O_BADGE = "PMS_NOTI_O_BADGE";
	String PREF_MQTT_POLLING_FLAG = "mqtt_polling_flag";
	String PREF_MQTT_POLLING_INTERVAL = "mqtt_polling_interval";
	String ACTION_POLLING = "MQTT.POLLING";
	// 대략 30분
	int INTERVAL_COUNT_16 = 16;
	String ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
	String ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
}
