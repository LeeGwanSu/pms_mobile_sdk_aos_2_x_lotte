package com.pms.sdk.common;

/**
 * Created by sijoonseong on 2016. 10. 13..
 */
public class PayloadNullException extends NullPointerException {
    /**
     * Constructs a new {@code PayloadNullException} that includes the current
     * stack trace.
     */
    public PayloadNullException() {
        super();
    }

    /**
     * Constructs a new {@code PayloadNullException} with the current stack
     * trace and the specified detail message.
     *
     * @param detailMessage the detail message for this exception.
     */
    public PayloadNullException(String detailMessage) {
        super(detailMessage);
    }

}
