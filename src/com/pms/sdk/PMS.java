package com.pms.sdk;

import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.view.Badge;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class) lotte 롯데닷컴
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.11 09:25] (lotte) uuid setting 추가<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.17 13:14] MQTT Client Service 중복 실행 체크 & ConnectionChangeReceiver.java 추가함. <br>
 *          [2013.12.18 17:46] (lotte) LoginPms시에 CustId가 같으면 API Call 안하게 변경.<br>
 *          [2013.12.19 09:02] MQTT Client ReConnect Timing 시간 변경 & ConectionChangeReceiver 변경 <br>
 *          [2013.12.19 17:35] MQTT Client Keep Alive 시간 랜덤으로 적용 & DeviceCert시 PrivateFlag 루틴 수정함. <br>
 *          [2013.12.27 13:08] GCM regist 시에 GCM_PACKAGE 전송 <br>
 *          [2013.12.27 14:53] DeviceCert 시 private Flag 처리 <br>
 *          [2014.01.10 13:29] MQTT Client 버그 사항 수정 및 안정화 <br>
 *          [2014.01.22 20:47] File Log 저장시 용량체크 후 삭제 <br>
 *          [2014.01.23 12:24] (lotte) DeviceCert시에 msgFlag, notiFlag를 업데이트 하지 않게 수정.<br>
 *          [2014.02.04 15:14] MQTT Client 파워 메니저 부분 수정.<br>
 *          [2014.02.05 16:36] 야간 모드 적용 & 로그 저장 루틴 추가<br>
 *          [2014.02.07 17:33] CollectLog 적용 부분 수정. <br>
 *          [2014.03.13 14:15] MQTT KeepAlive 부분 수정.<br>
 *          [2014.04.11 10:03] Notification 이벤트 적용.<br>
 *          [2014.06.09 11:11] 다른앱 실행시 팝업창 미노출 & Flag 추가함. <br>
 *          [2014.06.25 09:22] 예외처리 추가 & 팝업창 미노출 값 추가함. <br>
 *          [2014.07.30 10:17] Notification bar 우선순위 적용. <br>
 *          [2014.07.30 15:34] Notification bar 이벤트 삭제. <br>
 *          [2014.08.08 14:56] MQTT 버그 수정함. <br>
 *          [2014.08.21 11:45] API 호출시 마다 Cache Clear <br>
 *          [2014.09.11 11:00] Notification Icon을 블러오는 위치를 Androidmanifest.xml 쪽으로 바꿈. <br>
 *          [2014.10.22 11:15] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정 & MQTT Client 최신버전으로 교체함.<br>
 *          [2015.03.18 17:25] 팝업창 안뜨게 수정함.<br>
 *          [2015.06.29 16:34] 로그파일 저장 안되게 수정함.<br>
 *          [2015.10.13 15:46] 마케팅 수신동의 플래그 추가함.<br>
 *          [2016.03.09 13:15] 마케팅 수신동의 플래그 변경된점 추가함..<br>
 *          [2016.03.18 14:08] infoFlag값 추가함.<br>
 *          [2016.04.04 13:05] getSignKey 추가 및 새로 수정된 마케팅 플래그 추가함.<br>
 *          [2016.04.26 13:37] Permission 체크하는 로직 삭제함.<br>
 *          [2016.05.02 16:37] 앱 초기설치시 PushToken값을 못가져오는 현상 수정함.<br>
 *          [2016.07.19 13:41] Login항상 호출 할 수 있도롯 수정함. Notification Icon Back Color지정하도록 수정.<br>
 *          [2016.07.22 10:58] Login 부분 다시 원복함.<br>
 *          [2016.08.09 12:55] Private Server Reconnect 10 -> 1분으로 수정함.<br>
 *          [2016.09.20 16:19] Private Server Reconnect 1 -> 5분으로 수정함.<br>
 *          [2016.10.13 10:56] PayloadNull Exception 처리<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2017.02.14 16:10] 정보통신망법 관련하여 전화권한에 따른 수신거부 기능 개선 (DeviceCert, Notification) PhoneState Permission 체크<br>
 *          [2017.03.17 09:42] 정보통신망법 관련하여 전화권한에 따른 수신거부 기능에서 PushReceiver 원복<br>
 *          [2017.06.12 13:22] logout관련 처리 부분 수정<br>
 *          [2017.06.15 17:48] 두손가락을 이용해달라는 멘트나오지 않는 문제<br>
 *          [2017.09.13 17:16] PMSDB에 Data DB 추가 및 UUID 데이터 DB에 저장<br>
 *          [2017.10.23 11:45] Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					DB 중복 Open으로 인해 강제종료 문제 수정
 *          					PushPopup 발생 시 강제 종료 문제 수정
 *          [2018.02.21 17:52] * Android 8.0 대응 MQTTJobService 추가 (8.0 대응 추가 되면서 버전을 2.0.5 으로 변경 8.0 대응 버전 2.0.5으로 통일 예정)
 *          					Android Target 을 25이상으로 할 경우 Notification 하기 전에 NotificationChannel 설정하도록 수정 <br>
 *         	[2018.03.07 18:15] * Android 8.0 대응 Android version 25이상에서 PnedingIntent 명시적 호출로 변경
 *         						 Permission 확인 구문 제거 및 UUID 생성 부분 삭제 (요구사항에 의해)<br>
 *          [2018.03.08 11:24] * APIManager 에서 Permission 확인 부분 삭제 (요청) <br>
 *          [2018.03.19 19:29] * PushReceiver에서 (두손가락) 문구 삭제 <br>
 *          [2018.04.11 20:35] * CLog file 로그기능 추가 <br>
 *          [2018.07.11 11:08] Android 8.0 대응 (JobScheduler, NotificationChannel, Intent 제한)
 *          [2018.07.25 10:23] MQTTService startService intent null 예외처리
 *          [2018.09.13 15:14] Android 8.0 대응, 벨소리 대응, 잠금화면 체크, MQTT 간소화 적용, WRITE_STORAGE_PERMISSION 제거
 *          [2018.11.07 10:44] RestartReceiver 수정
 *          [2018.11.07 11:40] MQTT 수정
 *          [2019.02.07 10:28] FCM 적용, MQTTReceiver 적용
 *          [2019.03.08 18:41] FCM 액션 이름 수정, DeviceCert 비동기식 토큰 요청 오류 수정
 *          [2019.05.13 16:21] 업체 요청으로 Private 로직 제거, 알림채널 벨소리 동작 오류 수정, FCM 토큰 발급 개선, 사용자 리시버 액션값 입력할수 있도록 수정
 *          [2019.07.19 09:53] 불필요 소스 제거
 *          [2020.07.24 13:27] CustId 같을 경우 Login API 호출 막는 부분 제거
 */
public class PMS implements IPMSConsts {

	// auth status
	public enum AUTH {
		PENDING, PROGRESS, FAIL, COMPLETE;
	}

	private AUTH authStatus = AUTH.PENDING;

	private static PMS instance;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(final Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:202007241327");

		initOption(context);

		if(NO_TOKEN.equals(PMSUtil.getGCMToken(context)) || TextUtils.isEmpty(PMSUtil.getGCMToken(context)))
		{
			new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
				@Override
				public void callback(boolean isSuccess, String message)
				{
					CLog.i("FCMRequestToken isSuccess? "+isSuccess+" message : "+message);
				}
			}).execute();
		}

		/**
		 * refresh db thread
		 */
//		new Thread(new Runnable() {
//			@Override
//			public void run () {
//				CLog.i("refreshDBHandler:start");
//				mDB.deleteExpireMsg();
//				mDB.deleteEmptyMsgGrp();
//				if (mContext != null) {
//					mContext.sendBroadcast(new Intent(context, Badge.class).setAction(RECEIVER_REQUERY));
//				}
//				CLog.i("refreshDBHandler:end");
//			}
//		}).start();
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CANCEL_TEXT))) {
			mPrefs.putString(PREF_POPUP_CANCEL_TEXT, "CLOSE");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT))) {
			mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, "OK");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instance == null) {
			instance = new PMS(context);
		}
		instance.setmContext(context);
		return instance;
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instance.mContext, DEVICECERT_PENDING);
			instance.unregisterReceiver();
			instance = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	// /**
	// * push token 세팅
	// * @param pushToken
	// */
	// public void setPushToken(String pushToken) {
	// CLog.i(TAG + "setPushToken");
	// CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
	// PMSUtil.setGCMToken(mContext, pushToken);
	// }

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	/**
	 * set uuid
	 * 
	 * @param uuid
	 */
	public void setUuid (String uuid) {
		CLog.i("setUuid");
		CLog.d("setUuid:uuid=" + uuid);
		mPrefs.putString(PREF_UUID, uuid);
	}

	/**
	 * get Uuid
	 */
	public void getUuid () {
		CLog.i("getUuid");
		mPrefs.getString(PREF_UUID);
	}

	public void isGCMPush (Boolean isgcm) {
		PMSUtil.setIsGCMPush(mContext, isgcm);

	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * set pushPopup color
	 * 
	 * @param pushPopupColor
	 */
	public void setPushPopupColor (int pushPopupColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_COLOR, pushPopupColor);
	}

	/**
	 * set pushPopup background color
	 * 
	 * @param pushPopupBackgroundColor
	 */
	public void setPushPopupBackgroundColor (int pushPopupBackgroundColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_BACKGROUND_COLOR, pushPopupBackgroundColor);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * get mkt flag
	 * 
	 * @return
	 */
	public String getMktFlag () {
		return mPrefs.getString(PREF_MKT_FLAG);
	}

	/**
	 * get info flag
	 * 
	 * @return
	 */
	public String getInfoFlag () {
		return mPrefs.getString(PREF_INFO_FLAG);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}
	public void setPushReceiverAction (String intentAction) {
		mPrefs.putString(PREF_PUSH_RECEIVER, intentAction);
	}

	/**
	 * set noti receiver class
	 *
	 * @param intentClass
	 */
	public void setNotiReceiverClass (String intentClass) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
	}

	public void setPushReceiverClass (String intentClass) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentClass);
	}

	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * pms device 인증
	 * 
	 * @return
	 */
	public void authorize () {
		CLog.i("authorize:start");
		// auth status setting
		authStatus = AUTH.PROGRESS;

		new DeviceCert(mContext).request(new APICallback() {
			@Override
			public void response (String code, JSONObject json) {

				if (CODE_SUCCESS.equals(code)) {
					// success device cert
					CLog.i("DeviceCert:success device cert");
					authStatus = AUTH.COMPLETE;
				} else {
					// fail device cert
					CLog.i("DeviceCert:fail device cert");
					authStatus = AUTH.FAIL;
				}
			}
		});
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}
	
	public void setDebugFileMode (boolean debugFileMode) {
		CLog.setDebugFileMode(debugFileMode);
	}

	public void bindBadge (Context c, int id) {
		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge) {
		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			if (authStatus == AUTH.PENDING || authStatus == AUTH.FAIL) {
				// auth가 유효한 상태가 아니라면 authrize진행
				CLog.i("showMsgBox:no authorized");
				authorize();
			}

			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
		Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public Msg selectMsg (String userMsgId) {
		return mDB.selectMsg(userMsgId);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteMsg (String userMsgId) {
		return mDB.deleteMsg(userMsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */

	/**
	 * set popup confirm text
	 * 
	 * @param confirmText
	 */
	public void setPopupConfirmText (String confirmText) {
		mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, confirmText);
	}

	/**
	 * set popup cancel text
	 * 
	 * @param cancelText
	 */
	public void setPopupCancelText (String cancelText) {
		mPrefs.putString(PREF_POPUP_CANCEL_TEXT, cancelText);
	}

}
