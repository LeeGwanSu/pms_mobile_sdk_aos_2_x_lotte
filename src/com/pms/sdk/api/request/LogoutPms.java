package com.pms.sdk.api.request;

import android.content.Context;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class LogoutPms extends BaseRequest {

    public LogoutPms(Context context) {
        super(context);
    }

    /**
     * request
     *
     * @param apiCallback
     */
    public void request(final APICallback apiCallback) {
        try {
            PMSUtil.setCustId(mContext, "");
            mPrefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, "-1");

            mDB.deleteAll();

            apiManager.call(API_LOGOUT_PMS, new JSONObject(), new APICallback() {
                @Override
                public void response(String code, JSONObject json) {
                    if (CODE_SUCCESS.equals(code)) {
                        requiredResultProc(json);
                    }
                    if (apiCallback != null) {
                        apiCallback.response(code, json);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * required result proccess
     *
     * @param json
     */
    private boolean requiredResultProc(JSONObject json) {
        mPrefs.putString(PREF_LOGINED_STATE, FLAG_N);
        try {
            if (json.has("notiFlag"))
                mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
            if (json.has("mktFlag"))
                mPrefs.putString(PREF_MKT_FLAG, json.getString("mktFlag"));
            if (json.has("infoFlag"))
                mPrefs.putString(PREF_INFO_FLAG, json.getString("infoFlag"));
        } catch (JSONException e) {
            CLog.e(e.getMessage());
        }
        return true;
    }
}
