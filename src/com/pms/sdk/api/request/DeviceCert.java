package com.pms.sdk.api.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.view.Badge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert extends BaseRequest {

	public static final int CHECK_COUNT_LIMIT = 1;

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
//			jobj.put("uuid", "".equals(mPrefs.getString(PREF_UUID)) ? "noUuid" : mPrefs.getString(PREF_UUID));
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("sessCnt", "1");

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {

		String custId = PMSUtil.getCustId(mContext);

		int checkCnt = 0;
		// 권한이 없음으로 15번 재시도 필요없이 바로 패스
		// lotte 닷컴 요청으로 permission 삭제
//					boolean isExistsPermission = PhoneState.permissionChecker(mContext, Manifest.permission.READ_PHONE_STATE);
//					CLog.i("Device Cert request isExistsPermission : " + isExistsPermission);
//					if(!isExistsPermission) {
//						CLog.i("deviceCert:pushToken is null, sleep(2000)");
//						checkCnt = CHECK_COUNT_LIMIT;
//					}

		// UUID 생성구문 삭제 요청으로 UUID 생성 구문 삭제.
//					boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
//					CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

		// 앱이 처음 수행 됐을 때, push token을 받아오기 전 까지 device cert를 수행하지 않게
//		if (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) ||
//				NO_TOKEN.equals(PMSUtil.getGCMToken(mContext))) {
//			while (checkCnt < CHECK_COUNT_LIMIT &&
//					(StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(PMSUtil.getGCMToken(mContext)))) {
//				try {
//					CLog.i("deviceCert:pushToken is null, sleep(2000)");
//					checkCnt++;
//					Thread.sleep(2000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
//		}

		new FCMRequestToken(mContext, PMSUtil.getGCMProjectId(mContext), new FCMRequestToken.Callback() {
			@Override
			public void callback(boolean isSuccess, String message)
			{
				if(!isSuccess)
				{
					mPrefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
				}
				try {
					apiManager.call(API_DEVICE_CERT, getParam(), new APICallback() {
						@Override
						public void response (String code, JSONObject json) {
							if (CODE_SUCCESS.equals(code)) {
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
								requiredResultProc(json);
							}

							if (apiCallback != null) {
								apiCallback.response(code, json);
							}
						}
					});
				} catch (Exception e) {
					CLog.e(e.toString());
				}
			}
		}).execute();

		// if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID))) {
		// // 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
		// CLog.i("DeviceCert:new user");
		// mDB.deleteAll();
		// mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		// }

		CLog.i("DeviceCert:validate ok");
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));

			// set msg flag
			mPrefs.putString(PREF_API_LOG_FLAG, json.getString("collectApiLogFlag"));
			mPrefs.putString(PREF_PRIVATE_LOG_FLAG, json.getString("collectPrivateLogFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

			// set config flag
			String mflag = FLAG_N; 
			String nflag = FLAG_N;
			String mkflag = FLAG_N;
			String infoFlag = FLAG_N;
			
			try {
				mflag = json.getString("msgFlag");
			}
			catch (Exception e) {
				CLog.e(e.getMessage());
			}
			try {
				nflag = json.getString("notiFlag");
			}
			catch (Exception e) {
				CLog.e(e.getMessage());
			}
			try {
				mkflag = json.getString("mktFlag");
			}
			catch (Exception e) {
				CLog.e(e.getMessage());
			}
			try {
				infoFlag = json.getString("infoFlag");
			}
			catch (Exception e) {
				CLog.e(e.getMessage());
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
				mPrefs.putString(PREF_MSG_FLAG, mflag);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
				mPrefs.putString(PREF_NOTI_FLAG, nflag);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_MKT_FLAG))) {
				mPrefs.putString(PREF_MKT_FLAG, mkflag);
			}
			if (StringUtil.isEmpty(mPrefs.getString(PREF_INFO_FLAG))) {
				mPrefs.putString(PREF_INFO_FLAG, infoFlag);
			}

			if ((mPrefs.getString(PREF_MSG_FLAG).equals(mflag) == false) || (mPrefs.getString(PREF_NOTI_FLAG).equals(nflag) == false)
					|| (mPrefs.getString(PREF_MKT_FLAG).equals(mkflag) == false) || (mPrefs.getString(PREF_INFO_FLAG).equals(infoFlag) == false)) {
				new SetConfig(mContext).request(mflag, nflag, mkflag, infoFlag, null);
			}

			// set badge
			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

			// readMsg
			JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
			if (readArray.length() > 0) {
				// call readMsg
				new ReadMsg(mContext).request(null, readArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete readMsg
							new Prefs(mContext).putString(PREF_READ_LIST, "");
						}
					}
				});
			} else {
				CLog.i("readArray is null");
			}

			// clickMsg
			JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
			if (clickArray.length() > 0) {
				// call clickMsg
				new ClickMsg(mContext).request(null, clickArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete clickMsg
							new Prefs(mContext).putString(PREF_CLICK_LIST, "");
						}
					}
				});
			} else {
				CLog.i("clickArray is null");
			}

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext))) {
				String flag = json.getString("privateFlag");
				mPrefs.putString(PREF_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag)) {
					String protocol = json.getString("privateProtocol");
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext));
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
						} else {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
//							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
					}
//					mContext.sendBroadcast(new Intent(mContext, RestartReceiver.class).setAction(ACTION_START));
				} else {
//					mContext.stopService(new Intent(mContext, MQTTService.class));
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(mPrefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(mPrefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = mPrefs.getString(PREF_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			mPrefs.putBoolean(PREF_ONEDAY_LOG, false);
		}

		if (mPrefs.getBoolean(PREF_ONEDAY_LOG) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				mPrefs.putString(PREF_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
